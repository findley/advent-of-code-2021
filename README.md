# Advent of Code 2021
https://adventofcode.com/2021

## Organization
This is a library package and each daily challenge is a submodule. A test is
created for each challenge. Initially the test fails, but it is updated to pass
after the challenge is solved.

The input files are stored in the `data` directory.

## Running
Verify the solutions running tests

```
$ cargo test
```
