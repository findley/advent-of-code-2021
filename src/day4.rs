use std::collections::HashSet;
use std::error::Error;

use crate::util;

type Board = Vec<Vec<i64>>;

pub fn pt1(filename: &'static str) -> Result<i64, Box<dyn Error>> {
    let lines = util::read_lines(filename)?;
    let mut lines_iter = lines.iter();

    let number_line = lines_iter.next().ok_or("Missing numbers line")?;
    let numbers = parse_numbers(number_line.as_ref())?;
    let boards = parse_boards(lines_iter)?;

    let winner_score = run_first_winner(&numbers, &boards);

    Ok(winner_score.unwrap())
}

pub fn pt2(filename: &'static str) -> Result<i64, Box<dyn Error>> {
    let lines = util::read_lines(filename)?;
    let mut lines_iter = lines.iter();

    let number_line = lines_iter.next().ok_or("Missing numbers line")?;
    let numbers = parse_numbers(number_line.as_ref())?;
    let mut boards = parse_boards(lines_iter)?;

    let winner_score = run_last_winner(&numbers, &mut boards);

    Ok(winner_score.unwrap())
}

fn run_first_winner(numbers: &Vec<i64>, boards: &Vec<Board>) -> Option<i64> {
    let mut called = HashSet::<i64>::new();

    for num in numbers.iter() {
        called.insert(*num);
        for board in boards.iter() {
            if board_is_winner(board, &called) {
                return Some(*num * calc_board_score(board, &called));
            }
        }
    }

    return None;
}

fn run_last_winner(numbers: &Vec<i64>, boards: &mut Vec<Board>) -> Option<i64> {
    let mut called = HashSet::<i64>::new();

    for num in numbers.iter() {
        called.insert(*num);

        let winners: Vec<Board> = boards
            .drain_filter(|b| {
                return board_is_winner(b, &called);
            })
            .collect();

        if boards.is_empty() && winners.len() > 0 {
            return Some(*num * calc_board_score(&winners[0], &called));
        }
    }

    return None;
}

fn board_is_winner(board: &Board, called: &HashSet<i64>) -> bool {
    for i in 0..5 {
        if called.contains(&board[i][0])
            && called.contains(&board[i][1])
            && called.contains(&board[i][2])
            && called.contains(&board[i][3])
            && called.contains(&board[i][4])
        {
            return true;
        }

        if called.contains(&board[0][i])
            && called.contains(&board[1][i])
            && called.contains(&board[2][i])
            && called.contains(&board[3][i])
            && called.contains(&board[4][i])
        {
            return true;
        }
    }

    return false;
}

fn calc_board_score(board: &Board, called: &HashSet<i64>) -> i64 {
    let mut sum = 0;
    for row in board.iter() {
        for val in row.iter() {
            if !called.contains(val) {
                sum += *val;
            }
        }
    }

    return sum;
}

fn parse_numbers(line: &str) -> Result<Vec<i64>, Box<dyn Error>> {
    Ok(line
        .split(",")
        .map(|val| val.parse::<i64>().unwrap())
        .collect::<Vec<i64>>())
}

fn parse_boards(lines: std::slice::Iter<'_, String>) -> Result<Vec<Board>, Box<dyn Error>> {
    let mut boards: Vec<Board> = Vec::new();
    let mut current_board: Option<Board> = None;

    for line in lines {
        match (line.is_empty(), &mut current_board) {
            (true, Some(board)) => {
                boards.push(board.to_vec());
                current_board = None;
            }
            (false, None) => {
                current_board = Some(vec![parse_board_row(&line)?]);
            }
            (false, Some(board)) => {
                board.push(parse_board_row(&line)?);
            }
            _ => {}
        };
    }

    match current_board {
        Some(board) => boards.push(board),
        None => {}
    }

    return Ok(boards);
}

fn parse_board_row(line: &str) -> Result<Vec<i64>, Box<dyn Error>> {
    Ok(line
        .split_whitespace()
        .map(|val| val.parse::<i64>().unwrap())
        .collect::<Vec<i64>>())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_day4pt1_demo() {
        assert_eq!(4512, pt1("./data/day4_demo").unwrap());
    }

    #[test]
    fn test_day4pt1() {
        assert_eq!(54275, pt1("./data/day4").unwrap());
    }

    #[test]
    fn test_day4pt2() {
        assert_eq!(13158, pt2("./data/day4").unwrap());
    }
}
