use std::error::Error;

use crate::util;

pub fn pt1(filename: &'static str) -> Result<i64, Box<dyn Error>> {
    let lines = util::read_lines(filename)?;

    let depths = lines
        .iter()
        .map(|line| line.parse::<i64>().unwrap());

    let (increases, _) = depths.fold((0, None), |(inc, last_depth), depth| {
        let is_increase = last_depth.map_or(false, |last| depth > last);
        match is_increase {
            true => (inc + 1, Some(depth)),
            false => (inc, Some(depth)),
        }
    });

    Ok(increases)
}

pub fn pt2(filename: &'static str, window_size: usize) -> Result<i64, Box<dyn Error>> {
    let lines = util::read_lines(filename)?;

    let depths: Vec<_> = lines
        .iter()
        .map(|line| line.parse::<i64>().unwrap())
        .collect();

    let mut last_window = None;
    let mut inc = 0;

    for i in (window_size - 1)..depths.len() {
        let first = i - (window_size - 1);
        let window: i64 = depths[first..=i].iter().sum();

        inc += match last_window.map_or(false, |last| window > last) {
            true => 1,
            false => 0,
        };

        last_window = Some(window);
    }

    Ok(inc)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_day1pt1() {
        assert_eq!(1583, pt1("./data/day1").unwrap());
    }

    #[test]
    fn test_day1pt2() {
        assert_eq!(1627, pt2("./data/day1", 3).unwrap());
    }
}
