use std::{error::Error, str::FromStr};

use crate::util;

#[derive(Debug, Clone, Copy)]
struct Signal {
    wires: u8,
}

type DecMap = [u8; 10];

pub fn pt1(filename: &'static str) -> Result<usize, Box<dyn Error>> {
    let lines = util::read_lines(filename)?;

    let mut num_signals = 0;

    for line in lines.iter() {
        let (_unique_signals, output_signals) = parse_line(line);
        for sig in output_signals.iter() {
            if sig.is_1() || sig.is_4() || sig.is_7() || sig.is_8() {
                num_signals += 1;
            }
        }
    }

    Ok(num_signals)
}

pub fn pt2(filename: &'static str) -> Result<usize, Box<dyn Error>> {
    let lines = util::read_lines(filename)?;

    let mut total = 0;

    for line in lines.iter() {
        let (mut unique_signals, output_signals) = parse_line(line);
        let dec_map = decode_unique_signals(&mut unique_signals);

        let output: usize = output_signals
            .iter()
            .map(|sig| lookup_digit(&dec_map, sig))
            .zip([1000, 100, 10, 1])
            .map(|(a, b)| a * b)
            .sum();

        total += output;
    }

    Ok(total)
}

fn lookup_digit(dec_map: &DecMap, sig: &Signal) -> usize {
    let mut digit = 0;
    for (i, wires) in dec_map.iter().enumerate() {
        if sig.wires == *wires {
            digit = i;
            break;
        }
    }

    return digit;
}

fn decode_unique_signals(unique: &mut Vec<Signal>) -> DecMap {
    let sig_1 = unique.drain_filter(|sig| sig.is_1()).next().unwrap();
    let sig_4 = unique.drain_filter(|sig| sig.is_4()).next().unwrap();
    let sig_7 = unique.drain_filter(|sig| sig.is_7()).next().unwrap();
    let sig_8 = unique.drain_filter(|sig| sig.is_8()).next().unwrap();

    // Signal with 6 wires that contains all wires from sig_4
    let sig_9 = unique
        .drain_filter(|sig| sig.num_active_wires() == 6 && sig.contains(&sig_4))
        .next()
        .unwrap();

    // Signal with 6 wires that contains all wires from sig_7
    let sig_0 = unique
        .drain_filter(|sig| sig.num_active_wires() == 6 && sig.contains(&sig_7))
        .next()
        .unwrap();

    // Last signal with 6 wires
    let sig_6 = unique
        .drain_filter(|sig| sig.num_active_wires() == 6)
        .next()
        .unwrap();

    // Signal with 5 wires that is a subset of wires from sig_6
    let sig_5 = unique
        .drain_filter(|sig| sig.num_active_wires() == 5 && sig_6.contains(sig))
        .next()
        .unwrap();

    // Signal with 5 wires that is a subset of wires from sig_6
    let sig_3 = unique
        .drain_filter(|sig| sig.num_active_wires() == 5 && sig_9.contains(sig))
        .next()
        .unwrap();

    // Last signal with 5 wires
    let sig_2 = unique
        .drain_filter(|sig| sig.num_active_wires() == 5)
        .next()
        .unwrap();

    [
        sig_0.wires,
        sig_1.wires,
        sig_2.wires,
        sig_3.wires,
        sig_4.wires,
        sig_5.wires,
        sig_6.wires,
        sig_7.wires,
        sig_8.wires,
        sig_9.wires,
    ]
}

fn parse_line(line: &String) -> (Vec<Signal>, Vec<Signal>) {
    if let [unique_sig_string, output_string] = line.split("|").collect::<Vec<&str>>()[..] {
        let output_signals = output_string
            .split_whitespace()
            .map(|word| word.parse::<Signal>().unwrap())
            .collect();

        let unique_signals = unique_sig_string
            .split_whitespace()
            .map(|word| word.parse::<Signal>().unwrap())
            .collect();

        return (unique_signals, output_signals);
    }

    panic!("Unexpected input line");
}

impl FromStr for Signal {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut sig = Signal { wires: 0 };

        let char_vec: Vec<char> = s.chars().collect();
        for c in char_vec {
            let wire = match c {
                'a' => Ok(1 << 0),
                'b' => Ok(1 << 1),
                'c' => Ok(1 << 2),
                'd' => Ok(1 << 3),
                'e' => Ok(1 << 4),
                'f' => Ok(1 << 5),
                'g' => Ok(1 << 6),
                _ => Err(()),
            }?;

            sig.wires |= wire;
        }

        Ok(sig)
    }
}

impl Signal {
    fn is_1(&self) -> bool {
        self.num_active_wires() == 2
    }

    fn is_4(&self) -> bool {
        self.num_active_wires() == 4
    }

    fn is_7(&self) -> bool {
        self.num_active_wires() == 3
    }

    fn is_8(&self) -> bool {
        self.num_active_wires() == 7
    }

    fn num_active_wires(&self) -> usize {
        let mut num = 0;
        for i in 0..7 {
            let w = 1 << i;
            if self.wires & w > 0 {
                num += 1;
            }
        }

        return num;
    }

    fn contains(&self, other: &Self) -> bool {
        (self.wires | other.wires) == self.wires
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_day8pt1_demo() {
        assert_eq!(26, pt1("./data/day8_demo").unwrap());
    }

    #[test]
    fn test_day8pt1() {
        assert_eq!(245, pt1("./data/day8").unwrap());
    }

    #[test]
    fn test_day8pt2_demo() {
        assert_eq!(61229, pt2("./data/day8_demo").unwrap());
    }

    #[test]
    fn test_day8pt2() {
        assert_eq!(983026, pt2("./data/day8").unwrap());
    }
}
