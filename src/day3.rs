use std::error::Error;

use crate::util;

pub fn pt1(filename: &'static str, width: usize) -> Result<u64, Box<dyn Error>> {
    let lines = util::read_lines(filename)?;

    let readings = lines
        .iter()
        .map(|line| i64::from_str_radix(&line, 2).unwrap());

    let mut counts = vec![0; width];
    let (counts, num_readings) = readings.fold((&mut counts, 0), |(counts, num), reading| {
        for i in 0..width {
            if (1 << i & reading) > 0 {
                counts[i] += 1;
            }
        }
        return (counts, num + 1);
    });

    let mut gamma: u64 = 0;
    for (i, count) in counts.iter().enumerate() {
        if *count > num_readings / 2 {
            gamma |= 1 << i
        }
    }
    let mask = (1 << width) - 1;
    let epsilon = (!gamma) & mask;

    Ok(gamma * epsilon)
}

pub fn pt2(filename: &'static str, width: usize) -> Result<u64, Box<dyn Error>> {
    let lines = util::read_lines(filename)?;

    let readings: Vec<u64> = lines
        .iter()
        .map(|line| u64::from_str_radix(&line, 2).unwrap())
        .collect();

    let oxy = decode(width, readings.clone(), |num_set, num_readings| {
        num_set * 2 >= num_readings
    })
    .ok_or("Failed to decode oxygen rating")?;

    let co2 = decode(width, readings.clone(), |num_set, num_readings| {
        num_set * 2 < num_readings
    })
    .ok_or("Failed to decode co2 rating")?;

    Ok(oxy * co2)
}

fn decode<F>(width: usize, mut readings: Vec<u64>, keep_set: F) -> Option<u64>
where
    F: Fn(usize, usize) -> bool,
{
    for bit in (0..width).rev() {
        let num_set = count_set_bits(&readings, bit);
        let num_readings = readings.len();

        drain_readings(&mut readings, bit, keep_set(num_set, num_readings));

        if readings.len() == 1 {
            return Some(readings[0]);
        }

        if readings.len() == 0 {
            return None;
        }
    }

    return None;
}

fn drain_readings(readings: &mut Vec<u64>, bit: usize, keep_set: bool) {
    readings.drain_filter(|reading| {
        let is_set = *reading & (1 << bit) > 0;
        if keep_set && !is_set {
            return true;
        }
        if !keep_set && is_set {
            return true;
        }

        return false;
    });
}

fn count_set_bits(readings: &[u64], bit: usize) -> usize {
    let mask = 1 << bit;
    readings
        .iter()
        .fold(0, |ones, reading| match reading & mask > 0 {
            true => ones + 1,
            false => ones,
        })
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_day3pt1() {
        assert_eq!(3242606, pt1("./data/day3", 12).unwrap());
    }

    #[test]
    fn test_day3pt2() {
        assert_eq!(4856080, pt2("./data/day3", 12).unwrap());
    }

    #[test]
    fn test_day3pt2_demo() {
        assert_eq!(230, pt2("./data/day3_demo", 5).unwrap());
    }
}
