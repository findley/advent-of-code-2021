use std::error::Error;

use crate::util;

#[derive(Debug)]
struct HeightMap {
    width: usize,
    data: Vec<u32>,
}

pub fn pt1(filename: &'static str) -> Result<u32, Box<dyn Error>> {
    let lines = util::read_lines(filename)?;

    let map = HeightMap::from_lines(lines);

    let risk = map.low_points().iter().map(|(_, h)| h + 1).sum();

    Ok(risk)
}

pub fn pt2(filename: &'static str) -> Result<usize, Box<dyn Error>> {
    let lines = util::read_lines(filename)?;
    let map = HeightMap::from_lines(lines);
    let low_points = map.low_points();
    let mut visited = vec![false; map.data.len()];

    let mut basin_sizes: Vec<usize> = low_points
        .iter()
        .map(|(point, _)| map.basin_size(*point, &mut visited))
        .collect();

    basin_sizes.sort();
    let top_3_sizes = basin_sizes.iter().rev().take(3).product();

    Ok(top_3_sizes)
}

impl HeightMap {
    fn from_lines(lines: Vec<String>) -> Self {
        let data: Vec<u32> = lines
            .iter()
            .map(|line| line.chars().map(|ch| ch.to_digit(10).unwrap()))
            .flatten()
            .collect();

        return HeightMap {
            width: lines[0].len(),
            data,
        };
    }

    fn iter_kern(&self) -> KernelIter {
        KernelIter { pos: 0, map: self }
    }

    fn low_points(&self) -> Vec<(usize, u32)> {
        self.iter_kern()
            .filter(|((_, curr_h), neighbors)| {
                neighbors.iter().fold(true, |c, n| {
                    c && n.map(|(_, h)| *curr_h < h).unwrap_or(true)
                })
            })
            .map(|(pt, _)| pt)
            .collect()
    }

    fn neighbors(&self, i: usize) -> Neighbors {
        let i_up = i.checked_sub(self.width);
        let i_right = match (i + 1) % self.width {
            0 => None,
            _ => Some(i + 1),
        };
        let i_down = i + self.width;
        let i_left = match i % self.width {
            0 => None,
            _ => i.checked_sub(1),
        };

        [
            i_up.and_then(|i| self.data.get(i).map(|h| (i, *h))),
            i_right.and_then(|i| self.data.get(i).map(|h| (i, *h))),
            self.data.get(i_down).map(|h| (i_down, *h)),
            i_left.and_then(|i| self.data.get(i).map(|h| (i, *h))),
        ]
    }

    // Do a DFS from the given point stoping on height 9 or visited points.
    fn basin_size(&self, pt: usize, visited: &mut Vec<bool>) -> usize {
        let mut basin_size = 1;
        let mut stack = vec![pt];
        visited[pt] = true;

        while let Some(curr) = stack.pop() {
            let neighbors = self.neighbors(curr);

            for neighbor in neighbors.iter() {
                if let Some((i, h)) = neighbor {
                    if *h < 9 && !visited[*i] {
                        basin_size += 1;
                        stack.push(*i);
                        visited[*i] = true;
                    }
                }
            }
        }

        return basin_size;
    }
}

struct KernelIter<'a> {
    pos: usize,
    map: &'a HeightMap,
}

type Neighbors = [Option<(usize, u32)>; 4];

impl<'a> Iterator for KernelIter<'a> {
    type Item = ((usize, u32), Neighbors);

    fn next(&mut self) -> Option<Self::Item> {
        if self.pos >= self.map.data.len() {
            return None;
        }

        let neightbors = self.map.neighbors(self.pos);
        let i = self.pos;
        self.pos += 1;

        Some(((i, self.map.data[i]), neightbors))
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_day9pt1_demo() {
        assert_eq!(15, pt1("./data/day9_demo").unwrap());
    }

    #[test]
    fn test_day9pt1() {
        assert_eq!(444, pt1("./data/day9").unwrap());
    }

    #[test]
    fn test_day9pt2_demo() {
        assert_eq!(1134, pt2("./data/day9_demo").unwrap());
    }

    #[test]
    fn test_day9pt2() {
        assert_eq!(1134, pt2("./data/day9").unwrap());
    }
}
