use std::error::Error;

use crate::util;

#[derive(Debug)]
struct Population {
    num_by_state: Vec<u64>,
    days_to_replicate: u8,
}

pub fn pt1(
    filename: &'static str,
    days_to_replicate: u8,
    days_to_mature: u8,
    num_days: usize,
) -> Result<u64, Box<dyn Error>> {
    let lines = util::read_lines(filename)?;

    let mut pop = Population::new(days_to_replicate, days_to_mature);

    let initial_fish = parse_fish(&lines[0]);
    for fish in initial_fish.iter() {
        pop.num_by_state[*fish] += 1;
    }

    for _ in 0..num_days {
        pop.run();
    }

    Ok(pop.num_by_state.iter().sum())
}

fn parse_fish(line: &String) -> Vec<usize> {
    line.split(",")
        .map(|v| v.parse::<usize>().unwrap())
        .collect()
}

impl Population {
    fn new(days_to_replicate: u8, days_to_mature: u8) -> Self {
        Self {
            num_by_state: vec![0; (days_to_replicate + days_to_mature).into()],
            days_to_replicate,
        }
    }

    fn run(&mut self) {
        let num_new = self.num_by_state[0];

        for i in 0..self.num_by_state.len() {
            if i == self.num_by_state.len() - 1 {
                self.num_by_state[i] = num_new;
            } else {
                self.num_by_state[i] = self.num_by_state[i + 1];
            }
        }
        self.num_by_state[(self.days_to_replicate - 1) as usize] += num_new;
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_day6pt1_demo() {
        assert_eq!(5934, pt1("./data/day6_demo", 7, 2, 80).unwrap());
    }

    #[test]
    fn test_day6pt1() {
        assert_eq!(346063, pt1("./data/day6", 7, 2, 80).unwrap());
    }

    #[test]
    fn test_day6pt2_demo() {
        assert_eq!(26984457539, pt1("./data/day6_demo", 7, 2, 256).unwrap());
    }

    #[test]
    fn test_day6pt2() {
        assert_eq!(1572358335990, pt1("./data/day6", 7, 2, 256).unwrap());
    }
}
