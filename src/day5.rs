use std::collections::HashMap;
use std::error::Error;

use crate::util;

type Point = (i64, i64);

#[derive(Debug)]
struct Line(Point, Point);

pub fn pt1(filename: &'static str) -> Result<usize, Box<dyn Error>> {
    let file_lines = util::read_lines(filename)?;
    let mut point_counts = HashMap::<Point, i64>::new();

    file_lines
        .iter()
        .map(|line_result| parse_line(&line_result))
        .filter(|line| !line.is_diag())
        .for_each(|line| line.set_points(&mut point_counts));

    Ok(count_dangerous_points(&point_counts))
}

pub fn pt2(filename: &'static str) -> Result<usize, Box<dyn Error>> {
    let file_lines = util::read_lines(filename)?;
    let mut point_counts = HashMap::<Point, i64>::new();

    file_lines
        .iter()
        .map(|line_result| parse_line(&line_result))
        .for_each(|line| line.set_points(&mut point_counts));

    Ok(count_dangerous_points(&point_counts))
}

fn count_dangerous_points(points: &HashMap<Point, i64>) -> usize {
    points.iter().filter(|(_, count)| **count >= 2).count()
}

fn parse_line(line: &String) -> Line {
    let mut parts = line.split_whitespace();
    let pt1 = parts.next().unwrap();
    parts.next();
    let pt2 = parts.next().unwrap();

    Line(parse_point(pt1), parse_point(pt2))
}

fn parse_point(s: &str) -> Point {
    let mut nums = s.split(",").map(|v| v.parse::<i64>().unwrap());

    (nums.next().unwrap(), nums.next().unwrap())
}

impl Line {
    fn is_diag(&self) -> bool {
        self.0 .0 != self.1 .0 && self.0 .1 != self.1 .1
    }

    fn set_points(&self, points: &mut HashMap<Point, i64>) {
        let dx = self.0 .0 - self.1 .0;
        let dy = self.0 .1 - self.1 .1;

        if dx == 0 {
            let m = dx / dy;
            let y_low = self.0 .1.min(self.1 .1);
            let y_high = self.0 .1.max(self.1 .1);

            for y in y_low..=y_high {
                let x = m * (y - self.0 .1) + self.0 .0;
                *points.entry((x, y)).or_insert(0) += 1;
            }
        } else {
            let m = dy / dx;
            let x_low = self.0 .0.min(self.1 .0);
            let x_high = self.0 .0.max(self.1 .0);

            for x in x_low..=x_high {
                let y = m * (x - self.0 .0) + self.0 .1;
                *points.entry((x, y)).or_insert(0) += 1;
            }
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_day5pt1_demo() {
        assert_eq!(5, pt1("./data/day5_demo").unwrap());
    }

    #[test]
    fn test_day5pt1() {
        assert_eq!(5169, pt1("./data/day5").unwrap());
    }

    #[test]
    fn test_day5pt2_demo() {
        assert_eq!(12, pt2("./data/day5_demo").unwrap());
    }

    #[test]
    fn test_day5pt2() {
        assert_eq!(22083, pt2("./data/day5").unwrap());
    }
}
