use std::error::Error;

use crate::util;

pub fn pt1(filename: &'static str) -> Result<usize, Box<dyn Error>> {
    let lines = util::read_lines(filename)?;

    let mut locations = parse_locations(&lines[0]);

    locations.sort();

    let target = locations[locations.len() / 2];

    let fuel_req = locations
        .iter()
        .fold(0, |total, location| total + location.abs_diff(target));

    Ok(fuel_req)
}

pub fn pt2(filename: &'static str) -> Result<usize, Box<dyn Error>> {
    let lines = util::read_lines(filename)?;

    let locations = parse_locations(&lines[0]);

    // quadratic parameters for sum of cost = 0.5(n*n + n)
    let a = locations.len() as f64;
    let b = locations
        .iter()
        .fold(0.0, |total, l| total + -(*l as f64) * 2.0 + 1.0) as f64;

    // Vertex of the quadratic
    let target = (-b / (2.0 * a)).round() as usize;

    // Also check nearest locations (something funny is happening with either
    // rounding or the fact that the solution was continuous but final result
    // must be discrete)
    let fuel_req = vec![target - 1, target, target + 1]
        .iter()
        .map(|t| calc_quadratic_fule_cost(*t, &locations))
        .min();

    Ok(fuel_req.unwrap())
}

fn calc_quadratic_fule_cost(target: usize, locations: &Vec<usize>) -> usize {
    locations.iter().fold(0, |total, location| {
        let diff = location.abs_diff(target as usize);
        total + diff * (diff + 1) / 2
    })
}

fn parse_locations(line: &String) -> Vec<usize> {
    line.split(",")
        .map(|v| v.parse::<usize>().unwrap())
        .collect()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_day7pt1_demo() {
        assert_eq!(37, pt1("./data/day7_demo").unwrap());
    }

    #[test]
    fn test_day7pt1() {
        assert_eq!(352997, pt1("./data/day7").unwrap());
    }

    #[test]
    fn test_day7pt2_demo() {
        assert_eq!(168, pt2("./data/day7_demo").unwrap());
    }

    #[test]
    fn test_day7pt2() {
        assert_eq!(101571302, pt2("./data/day7").unwrap());
    }
}
