use std::error::Error;

use crate::util;

#[derive(Debug, PartialEq)]
#[repr(u8)]
enum Char {
    OpenParen = b'(',
    CloseParen = b')',
    OpenBracket = b'[',
    CloseBracket = b']',
    OpenCurly = b'{',
    CloseCurly = b'}',
    OpenAngle = b'<',
    CloseAngle = b'>',
}

pub fn pt1(filename: &'static str) -> Result<u64, Box<dyn Error>> {
    let lines = util::read_lines(filename)?;

    let error_score = lines
        .iter()
        .filter_map(|line| parse_line(line).err())
        .map(|e| e.lint_points())
        .sum();

    Ok(error_score)
}

pub fn pt2(filename: &'static str) -> Result<u64, Box<dyn Error>> {
    let lines = util::read_lines(filename)?;

    let mut completion_scores: Vec<u64> = lines
        .iter()
        .filter_map(|line| parse_line(line).ok())
        .map(|comp| {
            let mut score = 0;
            for ch in comp.iter().rev() {
                score = score * 5 + ch.comp_points();
            }

            return score;
        })
        .collect();

    completion_scores.sort();
    let mid_score = completion_scores[completion_scores.len() / 2];

    Ok(mid_score)
}

fn parse_line(line: &String) -> Result<Vec<Char>, Char> {
    let mut stack = Vec::<Char>::new();
    let chars = line.chars().map(|ch| Char::from(ch).unwrap());

    for char in chars {
        if char.is_open() {
            stack.push(char.matching());
        } else {
            if let Some(expected_ch) = stack.pop() {
                if char != expected_ch {
                    return Err(char);
                }
            } else {
                return Err(char);
            }
        }
    }

    return Ok(stack);
}

impl Char {
    fn from(c: char) -> Result<Char, ()> {
        match c {
            '(' => Ok(Char::OpenParen),
            ')' => Ok(Char::CloseParen),
            '[' => Ok(Char::OpenBracket),
            ']' => Ok(Char::CloseBracket),
            '{' => Ok(Char::OpenCurly),
            '}' => Ok(Char::CloseCurly),
            '<' => Ok(Char::OpenAngle),
            '>' => Ok(Char::CloseAngle),
            _ => Err(()),
        }
    }

    fn is_open(&self) -> bool {
        match *self {
            Self::OpenParen => true,
            Self::OpenBracket => true,
            Self::OpenCurly => true,
            Self::OpenAngle => true,
            _ => false,
        }
    }

    fn matching(&self) -> Self {
        match *self {
            Self::OpenParen => Self::CloseParen,
            Self::OpenBracket => Self::CloseBracket,
            Self::OpenCurly => Self::CloseCurly,
            Self::OpenAngle => Self::CloseAngle,
            Self::CloseParen => Self::OpenParen,
            Self::CloseBracket => Self::OpenBracket,
            Self::CloseCurly => Self::OpenCurly,
            Self::CloseAngle => Self::OpenAngle,
        }
    }

    fn lint_points(&self) -> u64 {
        match *self {
            Self::CloseParen => 3,
            Self::CloseBracket => 57,
            Self::CloseCurly => 1197,
            Self::CloseAngle => 25137,
            _ => 0,
        }
    }

    fn comp_points(&self) -> u64 {
        match *self {
            Self::CloseParen => 1,
            Self::CloseBracket => 2,
            Self::CloseCurly => 3,
            Self::CloseAngle => 4,
            _ => 0,
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_day10pt1_demo() {
        assert_eq!(26397, pt1("./data/day10_demo").unwrap());
    }

    #[test]
    fn test_day10pt1() {
        assert_eq!(343863, pt1("./data/day10").unwrap());
    }

    #[test]
    fn test_day10pt2_demo() {
        assert_eq!(288957, pt2("./data/day10_demo").unwrap());
    }

    #[test]
    fn test_day10pt2() {
        assert_eq!(2924734236, pt2("./data/day10").unwrap());
    }
}
