use std::error::Error;
use std::fmt::Display;
use std::str::FromStr;

use crate::util;

pub fn pt1(filename: &'static str) -> Result<(i64, i64), Box<dyn Error>> {
    let lines = util::read_lines(filename)?;

    let moves = lines.iter().map(|line| line.parse::<Move>().unwrap());

    Ok(moves.fold((0, 0), |(x, y), m| match m {
        Move::Up(d) => (x, y - d),
        Move::Down(d) => (x, y + d),
        Move::Forward(d) => (x + d, y),
    }))
}

pub fn pt2(filename: &'static str) -> Result<(i64, i64, i64), Box<dyn Error>> {
    let lines = util::read_lines(filename)?;

    let moves = lines.iter().map(|line| line.parse::<Move>().unwrap());

    Ok(moves.fold((0, 0, 0), |(aim, x, y), m| match m {
        Move::Up(d) => (aim - d, x, y),
        Move::Down(d) => (aim + d, x, y),
        Move::Forward(d) => (aim, x + d, y + (d * aim)),
    }))
}

enum Move {
    Up(i64),
    Down(i64),
    Forward(i64),
}

impl FromStr for Move {
    type Err = MoveParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut parts = s.split(" ");
        let cmd = parts.next();
        let arg = parts.next();

        let amount = arg
            .map(|a| {
                a.parse::<i64>().or_else(|_| {
                    Err(MoveParseError::new(
                        "Move argument could not be parsed as i64",
                    ))
                })
            })
            .ok_or_else(|| MoveParseError::new("Move argument was not provided"))??;

        match cmd {
            Some("forward") => Ok(Move::Forward(amount)),
            Some("up") => Ok(Move::Up(amount)),
            Some("down") => Ok(Move::Down(amount)),
            _ => Err(MoveParseError::new("a")),
        }
    }
}

#[derive(Debug)]
struct MoveParseError {
    msg: &'static str,
}

impl MoveParseError {
    fn new(msg: &'static str) -> Self {
        Self { msg }
    }
}

impl Display for MoveParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Failed to parse move {}", self.msg)
    }
}

impl Error for MoveParseError {}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_day2pt1() {
        let (x, y) = pt1("./data/day2").unwrap();
        assert_eq!(1451208, x * y);
    }

    #[test]
    fn test_day2pt2() {
        let (_a, x, y) = pt2("./data/day2").unwrap();
        assert_eq!(1620141160, x * y);
    }
}
