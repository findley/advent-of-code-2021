use std::error::Error;
use std::fs::File;
use std::io::{BufRead, BufReader};

pub fn read_lines(filename: &'static str) -> Result<Vec<String>, Box<dyn Error>> {
    let file = File::open(filename)?;
    let reader = BufReader::new(file);

    Ok(reader
        .lines()
        .map(|line_result| line_result.unwrap())
        .collect())
}
